#!/bin/bash

# ==============================================================================
# FUNCTIONS - START
# ==============================================================================
VERSION='1.1.6'
BASEDIR="${2-$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)}"

run_rebrand()
{
    printf '%b%s%b%s%b\n' '\e[1;32m' '==> ' '\e[0m\033[1m' 'Rebrand...' '\e[m'

    mv youtube_dl hypervideo

    # Delete paragraph 'GENERATE TARBALL' before building
    sed -i "28,40d" README.md

    find . -type f \( \
	 -iname "*" \
	 ! -iname "build.sh" \
	 ! -iname "Makefile" \
	 ! -iname ".travis.yml" \
         ! -iname ".gitlab-ci.yml" \
	 ! -path "./.git*" \) \
	 -exec grep -rIl 'youtube_dl' {} + -exec sed -i 's|youtube_dl|hypervideo|g' {} \;

    find . -type f \( \
	 -iname "*" \
	 ! -iname "build.sh" \
	 ! -iname "Makefile" \
	 ! -iname "README.md" \
	 ! -iname ".travis.yml" \
         ! -iname ".gitlab-ci.yml" \
	 ! -path "./.git*" \) \
	 -exec grep -rIl 'YOUTUBE-DL' {} + -exec sed -i 's|YOUTUBE-DL|HYPERVIDEO|g' {} \;

    # Set current version
    sed -i "s|__version.*|__version__ = '${VERSION}'|" hypervideo/version.py
}

run_clean()
{
    printf '%b%s%b%s%b\n' '\e[1;32m' '==> ' '\e[0m\033[1m' 'Clean tempfiles...' '\e[m'
    rm -rfv -- hypervideo.1.temp.md hypervideo.1 hypervideo.bash-completion README.txt MANIFEST build/ dist/ .coverage cover/ hypervideo.zsh hypervideo.fish hypervideo/extractor/lazy_extractors.py *.dump *.part* *.ytdl *.info.json *.mp4 *.m4a *.flv *.mp3 *.avi *.mkv *.webm *.3gp *.wav *.ape *.swf *.jpg *.png CONTRIBUTING.md.tmp hypervideo.exe
    find . -name "*.pyc" -delete
    find . -name "*.class" -delete
}

run_make()
{
    printf '%b%s%b%s%b\n' '\e[1;32m' '==> ' '\e[0m\033[1m' 'Making hypervideo...' '\e[m'
    make hypervideo.tar.gz
}

run_copy()
{
    printf '%b%s%b%s%b\n' '\e[1;32m' '==> ' '\e[0m\033[1m' 'Genering build...' '\e[m'
    install -d -m755 build
    mv hypervideo.tar.gz "./build/hypervideo.tar.gz"
    cd build && tar xzf hypervideo.tar.gz &&
          mv hypervideo "hypervideo-${VERSION}" &&
          tar -czvf "hypervideo-${VERSION}.tar.gz" "hypervideo-${VERSION}" &&
          rm -rf "hypervideo-${VERSION}" hypervideo.tar.gz
    cd "$BASEDIR" || return
    printf '%b%s%b%s%b\n' '\e[1;32m' '==> ' '\e[0m\033[1m' 'Tarball in build/ directory' '\e[m'
}

# Restore directory, compatibility reasons
run_reset()
{
    printf '%b%s%b%s%b\n' '\e[1;32m' '==> ' '\e[0m\033[1m' 'Restore compatibility...' '\e[m'
    mv hypervideo youtube_dl
    printf '%b%s%b%s%b\n' '\e[1;32m' '==> ' '\e[0m\033[1m' 'Please execute: "git checkout ." for complete restore' '\e[m'
}

# ==============================================================================
# EXECUTION - START
# ==============================================================================
run_rebrand "$@" && run_clean "$@"

run_make "$@"

run_copy "$@" && run_reset "$@"
